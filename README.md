|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/docker/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/docker/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/docker/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/docker/commits/develop)

## Ansible Role
### **_docker_**

Setup Docker engine on servers under RedHat/CentOS, Debian/Ubuntu and Alpine Linux operating systems.

## Requirements

  - Ansible 2.5 and higher
  - 'python-pycurl' for apt modules.

## Role Variables

```
# TODO
```

## Dependencies

None.

## License

Apache v2.0

## Author Information

This role is created by Paul Durivage and modified by "ITSupportMe, LLC" company

